# BB Cloud UI Extension

## Install

1. Clone repo
1. Navigate to `chrome://extensions`
1. Click on "Load Unpacked"
1. Navigate to folder and select.

## Reference

See post: https://blog.lateral.io/2016/04/create-chrome-extension-modify-websites-html-css/

---

## What UI changes to BB cloud UI are there?

### Repo nav
- When collapsed you can see the icons
- Hover on icons shows tooltip
- PR number shows on PR icon
- Issue: The PR number badge only appears on first page load

### Sidebar (files/activity)
- Moved to the left
- Issue: Resizing sidebar drag is mirrored


### Approval button
- Once PR approved by you, the button turns green

---

## What's next?

- Show comments at the top above file changes