const expandBtn = document.querySelector('[data-qa-id="expand-collapse-button"]');
const navLinks = document.querySelectorAll('[data-testid="ContextualNavigation"] a');

// Override navbar collapse
expandBtn.addEventListener("click", (event) => {
  event.preventDefault();
  event.stopPropagation();

  document.body.classList.toggle('bb-ui-nav-collapsed');
});

// Set hover titles on nav links
navLinks.forEach(el => {
  el.setAttribute('title', el.innerText.trim());
});

// Show PR # on link
fetch('https://bitbucket.org/!api/2.0/repositories/86400/86400-mobile-app/pullrequests')
  .then(response => {
    if (response.status !== 200) {
      return;
    }

    response.json().then(data => {
      document
        .querySelector('a[title="Pull requests"] > div:first-child')
        .innerHTML += `<span class="bb-ui-pr-num">${data.size}</span>`;
    });
  });